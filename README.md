Small library written in Micropython that handles basic CSV functionality. Only implements a small subset of the Python standard library csv module. Can parse and write simple CSV files. It might not be able to handle all CSV files. 

Output is not necessarily identical to Python csv output, but tries to be. Please raise an issue with a minimal example if output is not correct. Only comma separated files are tested so far and there aren't as many options as in the csv module. 

Most of the work is handled by generators so it is reasonably memory efficient, but may fail in Micropython for CSV files that are sufficiently large. It's recommended to use the flags micropython -X heapsize=5m on the Unix port to increase heap size. 

Experimental release. Not suitable for production.

## Usage

The basics of the Python csv API are available:

    with open("foo.csv", "r") as fin:
        reader = ncsv.reader(fin)
        my_data = list(reader)

And the same for `csv.writer`. 

Also implemented are `read` and `write` functions, which allow for shorter code:

    my_data = ncsv.read("foo.csv")

and

    ncsv.writerows(my_data, "bar.csv")

## Licence 

Released under MIT Licence.