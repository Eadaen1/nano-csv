import os
import sys

from setuptools import setup


with open("README.md") as readme_file:
    long_description = readme_file.read()

setup(
    name="nano-csv-micropython-Eadaen",
    version="0.0.1",
    author="Eadaen",
    author_email="eadaen@protonmail.com",
    description="Small CSV package written in Micropython",
    license="MIT Licence",
    keywords="micropython csv io",
    url="https://http://codeberg.org/eadaen/pcsv-micropython",
    packages=["ncsv"],
    long_description_content_type="text/markdown",
    long_description=long_description,
    python_requires=">=3.4",
    classifiers=[
        "Development Status :: 1 - Planning",
        "Topic :: Software Development :: Libraries :: Python Modules",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: Implementation :: MicroPython",
        "Programming Language :: Python :: 3.4",
        "Operating System :: POSIX :: Linux",
        "Operating System :: POSIX :: BSD",
        "Operating System :: Unix"
    ],
)
