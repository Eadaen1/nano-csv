import unittest
try:
    import uos
    import ujson
except ImportError:  # allows testing in CPython
    import os as uos
    import json as ujson
from __init__ import read, write

# Copyright (c) 2020 Eadaen. MIT Licence.


CSV, JSON, TEST = ".csv", ".json", ".test"

TOP_DIR = uos.getcwd()


def get_examples(directory):
    uos.chdir(directory)
    out = {x.replace(CSV, '').replace(JSON, '')
           for x in uos.listdir() if not x.endswith(TEST)}
    uos.chdir("..")
    return sorted(out)


SYMMETRIC_EXAMPLES = get_examples("symmetric_examples")  # examples of properly formatted CSV from Python
READ_EXAMPLES = get_examples("read_examples")  # examples of potentially bad CSV that we still want to parse correctly.


def enter_dir(path):
    uos.chdir(TOP_DIR + "/" + path)
    try:
        uos.remove('*' + TEST)
    except FileNotFoundError:
        pass


def exit_dir():
    try:
        uos.remove('*' + TEST)
    except FileNotFoundError:
        pass
    uos.chdir(TOP_DIR)


class TestPythonCompatibility(unittest.TestCase):
    """Check that ncsv generates same output as Python's csv"""
    def check_read(self, examples, dirname):
        enter_dir(dirname)
        for example in examples:
            print(f"read:   {example}")
            csv_out = read(example + CSV)
            with open(example + JSON, "rb") as fin:
                json_out = ujson.load(fin)
            self.assertEqual(csv_out, json_out, msg=f"{dirname}/{example}")
        exit_dir()

    def check_write(self, examples, dirname):
        enter_dir(dirname)
        for example in examples:
            print(f"write:   {example}")
            with open(example + JSON, "rb") as fin:
                json_out = ujson.load(fin)
            write(json_out, example + TEST)
            self.assertIdenticalFiles(example + CSV, example + TEST)
        exit_dir()

    def assertIdenticalFiles(self, path1, path2):
        with open(path1, "r") as fin:
            s1 = fin.read()
        with open(path2, "r") as fin:
            s2 = fin.read()
        self.assertEqual(s1, s2)

    def test_read_symmetric(self):  # todo add files generated from Python csv.
        """Checks whether ncsv loads CSV files equivalently to Python (CPython output previously saved as JSON."""
        self.check_read(SYMMETRIC_EXAMPLES, "symmetric_examples")

    #def test_write_symmetric(self):
    #    """Checks whether write is the same as CPython."""
    #    self.check_write(SYMMETRIC_EXAMPLES, "symmetric_examples")

    def test_read_asymmetric(self):
        self.check_read(READ_EXAMPLES, "read_examples")


unittest.main()
