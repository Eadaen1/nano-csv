"""
Use Python3's csv module to parse example CSV files to work out what we should be aiming for.
"""
import os
import csv
import json


if __name__ == "__main__":
    init_path = os.getcwd()
    for dir in ("symmetric_examples", "read_examples"):
        os.chdir(init_path)
        os.chdir(dir)
        for file in os.listdir():
            if file.endswith(".csv"):
                print(file)
                with open(file, 'r') as fin:
                    data = list(csv.reader(fin))
                with open(file.replace(".csv", ".json"), "w") as fin:
                    fin.write(json.dumps(data))
            elif file.endswith(".test"):
                os.remove(file)
