"""
CSV parsing and writing module for Micropython.
"""
from collections.abc import Iterator

# Copyright (c) 2020 Eadaen. MIT Licence.


QUOTE = "\""
TWO_QUOTE = 2 * QUOTE
_ESCAPE_CHARS = {"\n", "\r", ",", "\t", QUOTE}


def _read_row_generator(str_gen, col_delimiter):
    """Reads string generator input. Outputs a generator of rows in string form. Splits on col_delimiter (\n)
    :param str_gen:
    :param col_delimiter:
    :return:
    """
    new_row = ""
    control = True
    for s in str_gen:
        if s == QUOTE:
            control = not control
        elif s == col_delimiter and control:
            yield new_row
            new_row = ""
            continue
        new_row += s
    yield new_row


def process_word(word, i):
    """Parses a single element from CSV, handles quotation marks and other processing requirements.
    :param word:
    return: Core of word, with CSV punctuation removed.
    """
    # stdlib csv module sometimes handles the first entry in each row differently
    # Weird, but this check allows us to emulate that behaviour.
    if i and word.startswith(" "):
        return word

    # Handle trivial cases.
    if len(word) <= 1:
        return word
    if word == TWO_QUOTE:
        return ""
    if QUOTE not in word:
        return word

    # Get rid of trivial characters on either end of word and recursively solve for smaller strings.
    if not word.startswith(QUOTE):
        return word[0] + process_word(word[1:], i)
    if not word.endswith(QUOTE):
        return process_word(word[:-1], i) + word[-1]

    # Handle base case of escaped CSV element: '"some_text""\n,some_other_text"'
    return word[1:-1].replace(TWO_QUOTE, QUOTE)


def read_split_row(s, delimiter):
    """
    Splits a row by delimiter, taking into account escaped delimiters within elements.
    :param s: Input row iterable.
    :param delimiter: Character to split row on.
    :return: List of unprocessed elements.
    """
    out_str = ""
    control = True
    for char in s:
        if char == delimiter:
            if control:
                out_str += "\r"
            else:
                out_str += delimiter
        else:
            out_str += char
            if char == QUOTE:
                control = not control
    return out_str.split("\r")


def _read_engine3(s, delimiter):
    """
    Parses a line of CSV-formatted text and returns a generator of parsed elements.
    :param s: Input stream, line of text.
    :param delimiter: Character to split elements on.
    :return: Generator of processed elements.
    """
    for i, word in enumerate(read_split_row(s, delimiter)):
        yield process_word(word, i)


def _has_escape_chars(s):
    """Check if s has characters that would make the element need quoting.
    :param s: str for checking.
    :return: True if there are escape characters in s, False otherwise.
    """
    s_ = set(s)
    return any(escape in s_ for escape in _ESCAPE_CHARS)


def _write_element(elem):
    """Converts value elem into a sting, with necessary quotes added for CSV specification.
    :param elem: An individual element to convert.
    :return: Element converted to CSV string.
    """
    if _has_escape_chars(elem):
        return QUOTE + str(elem).replace(QUOTE, TWO_QUOTE) + QUOTE
    else:
        return str(elem)


def _write_engine(row, row_delimiter, col_delimiter):
    """Converts a single row to CSV string.
    :param row: List of items to be converted to CSV row.
    :param row_delimiter: CSV delimiter, typically ",".
    :param col_delimiter: Character to add to end of row to delineate rows, typically \n.
    :return: CSV-formatted str of row.
    """
    for i in range(len(row)):
        row[i] = _write_element(row[i])
    return row_delimiter.join(row) + col_delimiter


def _split_lines(s):  # s is generator of string characters # s is str for testing
    """Split string by \n in a semi-smart way.
    param s:
    :return:
    """
    s = (char for char in s if char != "\r")  # remove all \r
    return _read_row_generator(s, col_delimiter="\n")


def _split_delimiter(s, delimiter):
    """Split line by delimiter in a semi-smart way.
    :param s: str or generator line to split
    :return: generator of processed elements.
    """
    return _read_engine3(s, delimiter)


def _csv_splitter(s, delimiter):
    """Splits a CSV string. Opposite action of write_engine.
    :param s: string or generator of chars to split
    :return: list of generators, each represents a processed row.
    """
    # remove last blank line to fix off by one error
    return [_split_delimiter(x, delimiter) for x in _split_lines(s)][:-1]


def file_gen(fin):
    """
    Turns an input file stream into a generator, with some extra processing.
    :param fin: File to read.
    :return: Generator output, characters from file.
    """
    def internal(fin_inner):
        prev_ = "\n"
        while True:
            new_char = fin_inner.read(1)
            if new_char == "":  # Fix to support both files that end with \r\n and those that don't.
                if prev_ != "\n":
                    yield "\n"
                return
            else:
                yield new_char

    inter = internal(fin)
    prev = ""
    for x in inter:
        prev = x
        yield x
    if prev != "\n":
        yield "\n"


class reader(Iterator):
    """Implementation of Python csv.reader API for compatibility with Python code."""

    def __init__(self, fin, delimiter=","):
        self.fin = fin
        self.delimiter = delimiter
        processing_stream = file_gen(self.fin)
        self.gen = iter(_csv_splitter(processing_stream, delimiter=self.delimiter))

    def __next__(self):
        return self.gen.__next__()


class writer:
    """Implementation of Python csv.writer API for compatibility with Python code."""

    def __init__(self, fin, delimiter=","):
        self.fin = fin
        self.delimiter = delimiter

    def writerows(self, data):
        for row in data:
            out_str = _write_engine(row, self.delimiter, "\r\n")
            self.fin.write(out_str)

    def writerow(self, row):
        """untested, might be broken"""
        out_str = _write_engine(row, self.delimiter, "\r\n")
        self.fin.write(out_str)


def read(filename, delimiter=","):
    """New API for module. Opens file, reads from it and parses CSV.
    :param filename: Filename to automatically open and read.
    :param delimiter: Delimiter to split CSV rows by.
    :return: List of lists, output representation of CSV file.
    """
    with open(filename, "r") as fin:
        csv_reader = reader(fin, delimiter=delimiter)
        return [list(x) for x in csv_reader]


def write(data, filename, delimiter=","):
    """Writes an iterable into CSV. Supports 2D objects that have indexable 2nd dimension."""
    with open(filename, "w") as fin:
        csv_writer = writer(fin, delimiter=delimiter)
        csv_writer.writerows(data)


# todo make sure system works with \t delimiter.
if __name__ == "__main__":
    file = "symmetric_examples/numbers.csv"
    print(read(file))
